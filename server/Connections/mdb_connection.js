var mariadb = require("mariadb");

module.exports = mariadb.createConnection(
{
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'express_db'
});