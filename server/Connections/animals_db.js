var mysql = require("promise-mysql");

module.exports = mysql.createConnection(
{
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_animalerie'
});