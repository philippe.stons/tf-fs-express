var connection = require('../Connections/animals_db');

exports.getAll = () => 
{
    return connection.then((conn) => 
    {
        return conn.query('SELECT id, breed_id, chip_number FROM animal');
    });
}

exports.getOneByID = (id) => 
{
    return connection.then((conn) => 
    {
        return conn.query('SELECT id, breed_id, chip_number FROM animal WHERE id=?', id);
    });
}

exports.getOneByIdAndNbr = (id, nbr) => 
{
    return connection.then((conn) => 
    {
        return conn.query('SELECT id, breed_id, chip_number FROM animal WHERE breed_id=? AND chip_number=?', [id, nbr]);
    });
}