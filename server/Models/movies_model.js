var movie_db = require('../Connections/movie_db');
var moment = require('moment');

exports.getAll = () => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('SELECT * FROM film');
    });
}

exports.getByID = (id) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('SELECT * FROM film WHERE id=?', id);
    });
}

exports.insertMovie = (movie) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('INSERT INTO film (title, description, annee_sortie) VALUES(? , ?, ?)', 
            [movie.title, movie.description, moment(movie.annee_sortie).format('YYYY-MM-DD')]);
    });
}

exports.deleteMovieByID = (id) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('DELETE FROM film WHERE id=?', id);
    });
}

exports.updateMovieByID = (id, movie) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('UPDATE film SET title=?, description=?, annee_sortie=? WHERE id=?', 
            [movie.title, movie.description, moment(movie.annee_sortie).format('YYYY-MM-DD'), id]);
    });
}