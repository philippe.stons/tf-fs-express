var movie_db = require('../Connections/movie_db');

exports.getAll = () => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('SELECT * FROM actor');
    });
}

exports.getByID = (id) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('SELECT * FROM actor WHERE id=?', id);
    });
}

exports.insertActor = (actor) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('INSERT INTO actor (first_name, last_name) VALUES(? , ?)', [actor.first_name, actor.last_name]);
    });
}

exports.deleteActorByID = (id) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('DELETE FROM actor WHERE id=?', id);
    });
}

exports.updateActorByID = (id, actor) => 
{
    return movie_db.then((conn) => 
    {
        return conn.query('UPDATE actor SET first_name=?, last_name=? WHERE id=?', [actor.first_name, actor.last_name, id]);
    });
}