var express = require('express');
var router = express.Router();

var demo_ctrl = require('../Controllers/demo_controller');

router.get('/', demo_ctrl.demoPage);
router.get('/animals/:id', demo_ctrl.byId);
router.get('/animals/:id/:nbr', demo_ctrl.byIdAndNbr);

module.exports = router;