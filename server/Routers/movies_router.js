var express = require('express');
var router = express.Router();
var movie_ctl = require('../Controllers/movie_controller')

router.get('/', movie_ctl.getAll);
router.get('/id/:id', movie_ctl.getByID);
router.post('/create', movie_ctl.createMovie);
router.delete('/:id', movie_ctl.deleteMovie);
router.put('/:id', movie_ctl.updateMovie);

module.exports = router;