var actors = require('../Models/actors_model');

class Actor
{
    id;
    first_name;
    last_name;

    constructor(data)
    {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
    }
}

exports.getAll = (req, res) => 
{
    actors.getAll().then((results) => 
    {
        res.json(results);
    });
}

exports.getByID = (req, res) => 
{
    actors.getByID(req.params.id).then((results) => 
    {
        res.json(results);
    });
}

exports.createActor = (req, res) => 
{
    try
    {
        var actor = new Actor(req.body);
        actors.insertActor(actor);
        actors.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch (err)
    {
        res.status(500).send(err);
    }
}

exports.deleteActor = (req, res) => 
{
    try
    {
        actors.deleteActorByID(req.params.id);
        actors.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch(err)
    {
        res.status(500).send(err);
    }
}

exports.updateActor = (req, res) => 
{
    try
    {
        var actor = new Actor(req.body);
        actors.updateActorByID(req.params.id, actor);
        actors.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch(err)
    {
        res.status(500).send(err);
    }
}