var movies = require('../Models/movies_model');

class Movie
{
    id;
    title;
    description;
    annee_sortie;

    constructor(data)
    {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.annee_sortie = data.annee_sortie;
    }
}

exports.getAll = (req, res) => 
{
    movies.getAll().then((results) => 
    {
        res.json(results);
    });
}

exports.getByID = (req, res) => 
{
    movies.getByID(req.params.id).then((results) => 
    {
        res.json(results);
    });
}

exports.createMovie = (req, res) => 
{
    try
    {
        var movie = new Movie(req.body);
        movies.insertMovie(movie);
        movies.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch (err)
    {
        res.status(500).send(err);
    }
}

exports.deleteMovie = (req, res) => 
{
    try
    {
        movies.deleteMovieByID(req.params.id);
        movies.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch(err)
    {
        res.status(500).send(err);
    }
}

exports.updateMovie = (req, res) => 
{
    try
    {
        var movie = new Movie(req.body);
        movies.updateMovieByID(req.params.id, movie);
        movies.getAll().then((results) => 
        {
            res.json(results);
        });
    } catch(err)
    {
        res.status(500).send(err);
    }
}