import { Router } from 'express';
import { MovieController } from '../Controllers/movie_controller'

export class MovieRouter
{
    public router: Router;

    constructor()
    {
        this.router = Router();

        this.router.get('/', MovieController.getAll);
        this.router.get('/id/:id', MovieController.getOneByID);
        this.router.post('/create', MovieController.createMovie);
        this.router.delete('/:id', MovieController.deleteMovie);
        this.router.put('/:id', MovieController.updateMovieByID);
    }
}