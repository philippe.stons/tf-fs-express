import { Router } from 'express';
import { ActorController } from '../Controllers/actor_controller'

export class ActorRouter
{
    public router: Router;

    constructor()
    {
        this.router = Router();

        this.router.get('/', ActorController.getAll);
        this.router.get('/id/:id', ActorController.getOneByID);
        this.router.post('/create', ActorController.createActor);
        this.router.delete('/:id', ActorController.deleteActor);
        this.router.put('/:id', ActorController.updateActorByID);
    }
}