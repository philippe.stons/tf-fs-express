import { Router, Request, Response, NextFunction } from 'express';
import { MovieModel, Movie } from '../Models/movie_model';

export namespace MovieController
{
    export async function getAll(req: Request, res: Response, next: NextFunction)
    {
        const results = await MovieModel.getAll();
        res.json(results);
    }

    export async function getOneByID(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const results = await MovieModel.getOneByID(req.params.id);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }

    export async function createMovie(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            console.log(req.body);
            const movie = new Movie(req.body);
            const results = await MovieModel.insertMovie(movie);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }

    export async function deleteMovie(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const results = await MovieModel.deleteMovieByID(req.params.id);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }
    
    export async function updateMovieByID(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const movie = new Movie(req.body);
            const results = await MovieModel.updateMovieByID(req.params.id, movie);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }
}