import { Router, Request, Response, NextFunction } from 'express';
import { ActorModel, Actor } from '../Models/actor_model';

export namespace ActorController
{
    export async function getAll(req: Request, res: Response, next: NextFunction)
    {
        const results = await ActorModel.getAll();
        res.json(results);
    }

    export async function getOneByID(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const results = await ActorModel.getOneByID(req.params.id);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }

    export async function createActor(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            console.log(req.body);
            const actor = new Actor(req.body);
            const results = await ActorModel.insertActor(actor);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }

    export async function deleteActor(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const results = await ActorModel.deleteActorByID(req.params.id);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }
    
    export async function updateActorByID(req: Request, res: Response, next: NextFunction)
    {
        try
        {
            const actor = new Actor(req.body);
            const results = await ActorModel.updateActorByID(req.params.id, actor);
            res.json(results);
        } catch(err)
        {
            res.status(500).send(err);
        }
    }
}