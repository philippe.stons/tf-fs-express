import * as mysql from 'promise-mysql'

export function connect()
{
    return mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'movie_node_express'
        });
}