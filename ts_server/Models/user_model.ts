import { connect } from '../Connections/movie_db';

export class User
{
    id: number;
    username: string;
    password: string;
    email: string;
    admin: boolean;

    constructor(data: any)
    {
        this.id = data.id;
        this.username = data.username;
        this.password = data.password;
        this.email = data.email;
        this.admin = data.admin ? data.admin : 0;
    }
}

export class UserModel
{
    public static async getAll()
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT id, username, email, admin FROM users').then((results) => 
            {
                return results;
            });
        });
    }

    public static async getOneByID(id: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT id, username, email, admin FROM users WHERE id=?', id).then((results) =>
            {
                return results;
            });
        });
    }

    public static async getOneByName(name: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT id, username, email FROM users WHERE username=?', name).then((results) =>
            {
                return results;
            });
        });
    }

    public static async getOneByEmail(email: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT id, username, email FROM users WHERE email=?', email).then((results) =>
            {
                return results;
            });
        });
    }

    public static async insertUser(user: User)
    {
        return connect().then((conn) => 
        {
            return conn.query('INSERT INTO users (username, password, email) VALUES(? , ?, ?)', 
            [user.username, user.password, user.email]).then((results) => 
            {
                return this.getAll();
            });
        });
    }

    public static async deleteUserByID(id: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('DELETE FROM users WHERE id=?', id).then((results) => 
            {
                return this.getAll();
            });
        });
    }

    public static async updatePassword(user: User)
    {
        return connect().then((conn) => 
        {
            return conn.query('UPDATE users SET password=? WHERE id=?', 
            [user.password, user.id]).then((results) => 
            {
                return this.getOneByID(user.id);
            });
        });
    }

    public static async updateUserByID(id: any, user: User)
    {
        return connect().then((conn) => 
        {
            return conn.query('UPDATE users SET username=?, email=?, admin=? WHERE id=?', 
            [user.username, user.email, user.admin, id]).then((results) => 
            {
                return this.getOneByID(id);
            });
        });
    }

    public static async checkPassword(username: string, password: string): Promise<any>
    {
        try
        {
            let res = await connect().then((conn) => 
            {
                return conn.query('SELECT id, username, password, email, admin FROM users WHERE username=?', username).then(
                    (results) =>
                    {
                        return results;
                    });
            });
    
            if(res[0].password === password)
            {
                return { success: true, admin: res[0].admin };
            }
        } catch(err)
        {
            console.error('[ERROR] checkPassword username : ' + username + ' password : ' + password);
            console.error(err);
        }
        return { success: false, admin: false };
    }
}