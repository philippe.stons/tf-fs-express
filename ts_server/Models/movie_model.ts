import { connect } from '../Connections/movie_db';
import * as moment from 'moment';

export class Movie
{
    id: number;
    title: string;
    description: string;
    annee_sortie: string;

    constructor(data: any)
    {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.annee_sortie = data.annee_sortie;
    }
}

export class MovieModel
{
    public static async getAll()
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT * FROM film').then((results) => 
            {
                return results;
            });
        });
    }

    public static async getOneByID(id: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('SELECT * FROM film WHERE id=?', id).then((results) =>
            {
                return results;
            });
        });
    }

    public static async insertMovie(movie: Movie)
    {
        return connect().then((conn) => 
        {
            return conn.query('INSERT INTO film (title, description, annee_sortie) VALUES(? , ?, ?)', 
            [movie.title, movie.description, moment(movie.annee_sortie).format('YYYY-MM-DD')]).then((results) => 
            {
                return this.getAll();
            });
        });
    }

    public static async deleteMovieByID(id: any)
    {
        return connect().then((conn) => 
        {
            return conn.query('DELETE FROM film WHERE id=?', id).then((results) => 
            {
                return this.getAll();
            });
        });
    }

    public static async updateMovieByID(id: any, movie: Movie)
    {
        return connect().then((conn) => 
        {
            return conn.query('UPDATE film SET title=?, description=?, annee_sortie=? WHERE id=?', 
            [movie.title, movie.description, moment(movie.annee_sortie).format('YYYY-MM-DD'), id]).then((results) => 
            {
                return this.getAll();
            });
        });
    }
}