var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var animalRouter = require('./server/Routers/animals_router');
var actorRouter = require('./server/Routers/actors_router');
var movieRouter = require('./server/Routers/movies_router');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/animals", animalRouter);
app.use("/api/movies/actors", actorRouter);
app.use("/api/movies/movie", movieRouter);

app.listen(8000);